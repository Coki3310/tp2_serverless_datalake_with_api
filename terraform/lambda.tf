# TODO : Create the lambda role with aws_iam_role
resource "aws_iam_role" "iam_policy" {
  name = "iam_policy"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

# TODO : Create lambda function with aws_lambda_function
resource "aws_lambda_function" "test_lambda" {
  filename      = "empty_lambda_code.zip"
  handler       = "lambda_main_app.lambda_handler"
  runtime       = "python3.7"
  function_name = "lambda_function_name"
  role          = aws_iam_role.iam_policy.arn
  source_code_hash = filebase64sha256("empty_lambda_code.zip")

  environment {
    variables = {
      foo = "bar"
    }
  }
}

# TODO : Create a aws_iam_policy for the logging, this resource policy will be attached to the lambda role

resource "aws_iam_policy" "lambda_logging" {
  name        = "lambda_logging"
  path        = "/"
  description = "IAM policy for logging from a lambda"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:*",
      "Effect": "Allow"
    }
  ]
}
EOF
}

# TODO : Attach the logging policy to the lamda role with aws_iam_role_policy_attachment
resource "aws_iam_role_policy_attachment" "lambda_logs" {
  role       = aws_iam_role.iam_policy.name
  policy_arn = aws_iam_policy.lambda_logging.arn
}

# TODO : Attach the AmazonS3FullAccess policy to the lambda role with aws_iam_role_policy_attachment
resource "aws_iam_role_policy_attachment" "s3_FullAccess" {
  role       = aws_iam_role.iam_policy.name
  policy_arn = aws_iam_policy.s3_FullAccess.arn
}
resource "aws_iam_policy" "s3_FullAccess" {
  name        = "s3_FullAccess"
  path        = "/"
  description = "IAM policy for full access to s3"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
          "Effect": "Allow",
          "Action": [
                "s3:*"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.job_offer.bucket}/*"
            ]
        }
  ]
}
EOF
}

# TODO : Allow the lambda to be triggered by a s3 event with aws_lambda_permission
resource "aws_lambda_permission" "s3-lambda-permission" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.test_lambda.function_name
  principal     = "s3.amazonaws.com"
  source_arn    = "arn:aws:s3:::${aws_s3_bucket.job_offer.bucket}"
}