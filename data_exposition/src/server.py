from flask import Flask, render_template
import pandas as pd

from athena_client import send_query_to_athena

app = Flask(__name__, template_folder="templates")


@app.route("/")
def hello():
    return "Welcome To Job Hunter!"


@app.route("/offers", methods=("POST", "GET"))
def job_offers():
    query = "select * from job_offers"
    athena_result_csv_path = send_query_to_athena(query)["Body"]
    df = pd.read_csv(athena_result_csv_path)
    return render_template("data.html", tables=[df.to_html(classes="data", header="true")])


def render_df_as_html_array(df):
    return render_template("data.html", tables=[df.to_html(classes="data", header="true")])


if __name__ == "__main__":
    app.run(host="0.0.0.0")
